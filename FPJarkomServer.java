/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpjarkomserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 *
 * @author Aleksis Xancez
 */
public class FPJarkomServer {

    /**
     * @param args the command line arguments
     */
    
    private static ServerSocket ss = null;
    private static Socket client = null;
    
    private static final int maxClientsCount = 10;
    private static final clientThread[] threads = new clientThread[maxClientsCount];
    
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        int portNumber = 4444;
        if (args.length < 1) {
            System.out.println("Usage: java MultiThreadChatServerSync <portNumber>\n"
                + "Now using port number=" + portNumber);
        } else {
            portNumber = Integer.valueOf(args[0]).intValue();
        }

        try{
            ss = new ServerSocket(portNumber);
        }catch(IOException e){
            System.out.println(e);
        }
        
        
        
        System.out.println("The server started. To stop it press <CTRL><C>.");
        try{
            client = ss.accept();
            DataInputStream dis = new DataInputStream(client.getInputStream()); 
            DataOutputStream dos = new DataOutputStream(client.getOutputStream());
            
            
            while (true) 
		{ 
			 
			String input = dis.readUTF();  

			if(input.equals("bye")) 
				break; 

			System.out.println("Input Client 1: " + input);
			int resClient_1;
 
			StringTokenizer st = new StringTokenizer(input);  
                        
			int oprnd1 = Integer.parseInt(st.nextToken()); 
			String operation = st.nextToken(); 
			int oprnd2 = Integer.parseInt(st.nextToken());

			if (operation.equals("+")) 
			{ 
                            resClient_1 = oprnd1 + oprnd2;
			} 

			else if (operation.equals("-")) 
			{ 
                            resClient_1 = oprnd1 - oprnd2; 
			} 
			else if (operation.equals("*")) 
			{ 
                            resClient_1 = oprnd1 * oprnd2; 
			} 
			else
			{ 
                            resClient_1 = oprnd1 / oprnd2; 
			} 
			System.out.println("Mengirim Hasil"); 

			dos.writeUTF(Integer.toString(resClient_1)); 
                }
        } catch(IOException e){
            System.out.println(e);
        }
    }
    
}
