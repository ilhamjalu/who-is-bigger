/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalprojectjarkom;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Aleksis Xancez
 */
public class FinalProjectJarkom {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        //MainView view = new MainView();
        //view.setVisible(true);
        
        InetAddress ip = InetAddress.getLocalHost(); 
        int port = 4444;
        Scanner sc = new Scanner(System.in); 
        
        Socket s = new Socket(ip, port);
        DataInputStream dis = new DataInputStream(s.getInputStream());
        DataOutputStream dos = new DataOutputStream(s.getOutputStream());
        
        while (true) 
		{ 
			System.out.print("Masukkan operand dan operator dengan format : "); 
			System.out.println("'operand operator operand'"); 

			String inp = sc.nextLine(); 

			if (inp.equals("bye")) 
				break; 
                        
			dos.writeUTF(inp); 

                        String ans = dis.readUTF(); 
			System.out.println("Jawaban =" + ans); 
		}

    }
    
}
